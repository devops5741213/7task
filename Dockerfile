# Используем базовый образ Maven для сборки приложения
FROM maven:3.9.0-eclipse-temurin-17-alpine AS build

# Устанавливаем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем необходимые файлы для сборки проекта
COPY pom.xml mvnw mvnw.cmd ./
COPY .mvn/wrapper ./.mvn/wrapper
COPY src ./src

# Выполняем сборку приложения с помощью Maven
RUN mvn clean package

# Используем базовый образ java17 
FROM eclipse-temurin:17-jre-alpine

WORKDIR /app

# Копируем скомпилированные файлы приложения из образа для сборки в финальный образ
COPY --from=build /app/target/*.jar app.jar
COPY --from=build /app/pom.xml .
COPY --from=build /app/src ./src

# Открываем порт 8080
EXPOSE 8080

# указываю команды для запуска приложения
ENTRYPOINT ["java", "-jar", "app.jar"]

